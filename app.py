import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc

# FONT_FAMILY = 'system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
# FONT_SIZE = 18
# GRAPH_COLORS='px.colors.qualitative.Dark2'

external_stylesheets = [dbc.themes.PULSE]
app = Dash(__name__, external_stylesheets=external_stylesheets, use_pages=True)

app.layout = dbc.Container([
    dbc.Row([
        html.H1('Analysis of Rumour Spreading on Twitter', className="text-primary text-center fs-1 app-header")
    ]),
    dbc.Row([
        dbc.Col([
            dbc.Nav(
                [
                    html.H4(
                        dbc.NavItem(dbc.NavLink(f"{page['name']}", active=True, href=page["relative_path"])),
                    )
                    for page in dash.page_registry.values()
                ],
                vertical="md",
                pills=True,
            )
        ], md=2),

        dbc.Col([
           dash.page_container
        ], md=8),

        
    ], className="app-block", justify="around"),



], fluid=True)


if __name__ == '__main__':
    app.run_server(host="0.0.0.0", debug=True)
