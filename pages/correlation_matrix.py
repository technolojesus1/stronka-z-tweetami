import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from functions.plots_style import style_graph, GRAPH_COLORS, rename_topics, switch, barmode

CSV_PATH = "csv"
topics = {
    "abortion": "Texas Restricting Abortion",
    "tiktok": "TikTok Ban",
    "drag": "Drag Ban",
    "trump": "Trump's Rape Case",
    "shooting": "Nashville School Shooting",
    "writers": "Writers Guild Strike"
    }
topics_keys = ["abortion", "tiktok", "drag", "trump", "shooting", "writers"]
full_topics = []

dict = {}

for topic in topics.keys():
    df_small = pd.read_csv(f"{CSV_PATH}/{topic}_correlation_matrix.csv")
    df_small = df_small.loc[:, ~df_small.columns.isin(['punctuation', 'emojis', 'sizes'])]
    df_small = df_small.drop([7, 8, 9])
    df_small = df_small.loc[:, df_small.columns != 'Unnamed: 0']
    # print(df_small)
    dict[topic] = df_small
    # full_topics.append(topics[topic])

# df = pd.DataFrame(data = dict, index = TOPICS)
# print(df)
# rename_topics(df)

dash.register_page(__name__)

layout = dbc.Container([
    dbc.Row([
        html.H2('Correlation Matrix', className="text-secondary text-center fs-2")
    ]),
    

    dbc.Row([
        dcc.RadioItems(
            id=f'checklist-matrix',
            className="__checklist",
            options=topics_keys,
            value='drag',
            inline=True,
        ),
    ]),

    dbc.Row([
        dcc.Graph(id=f'matrix')
    ]),


], fluid=True)


@callback(
    Output(f'matrix', 'figure'),
    Input(f'checklist-matrix', 'value'),
)
def update_graph(topic):
    df = dict[topic]
    column_names = df.columns.values.tolist()
    print(column_names)
    fig = px.imshow(df,
                    x=column_names,
                    y=column_names,
                    zmin=-0.3, zmax=1,
                    color_continuous_scale='Blackbody_r',
                    )
    fig.update_xaxes(side="top")
    fig.update_layout(
    height = 700,
    width = 1000,
    autosize = True 
    )
    x_title = f"{topics[topic]} - Correlation Matrix"
    style_graph(fig, x_title)
    return fig

