import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import numpy as np
import dash_bootstrap_components as dbc



dash.register_page(__name__, path='/', order=0)


layout = dbc.Container([
    
    dbc.Row([
        dbc.Col([
            dbc.Row([html.H2('Team', className="text-secondary text-center fs-2")]),
            dbc.Row([html.H3('Jagoda Bracha', className="text-center")]),
            dbc.Row([html.H3('Lara Citko', className="text-center")]),
            dbc.Row([html.H3('Jan Ossowski', className="text-center")]),
            dbc.Row([html.H3('Marcin Pawłowski', className="text-center")]),
        ], md=5),

        dbc.Col([
            dbc.Stack([
                dbc.Row([
                    dbc.Row([
                        html.H2('Company', className="text-secondary text-center fs-2")
                    ]),
                    dbc.Row([
                        html.H3('MIM Solutions', className="text-center")
                    ]),
                ], className="app-block", justify="center"),

                dbc.Row([
                    dbc.Row([
                        html.H2('Supervisor', className="text-secondary text-center fs-2")
                    ]),
                    dbc.Row([
                        html.H3('mgr Łukasz Kamiński', className="text-center")
                    ]),
                ], className="app-block", justify="center"),
            ], gap=3)
        ], md=5),
    ]),

], fluid=True, className="pad-row")




