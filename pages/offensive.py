import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from functions.plots_style import style_graph, GRAPH_COLORS, rename_topics, switch, barmode

CSV_PATH = "csv"
TOPIC = "offensive"

df = pd.read_csv(f"{CSV_PATH}/{TOPIC}.csv")
rename_topics(df)

dash.register_page(__name__, order=6)

layout = dbc.Container([
    dbc.Row([
        html.H2('P Value and Offensiveness', className="text-secondary text-center fs-2")
    ]),
    

    dbc.Button('Select all', id='select-all'),
    dbc.Row([
        dcc.Checklist(
            id=f'checklist-{TOPIC}',
            className="__checklist",
            options=df.topic.unique(),
            value=['Drag Ban'],
            inline=True,
        ),
    ]),

    dbc.Row([
        dcc.Graph(id=f'graph-p-{TOPIC}')
    ]),

    dbc.Row([
        dcc.Graph(id=f'graph-count-{TOPIC}')
    ]),

    switch("views"),
    barmode(),


], fluid=True)

@callback(
    Output(f'checklist-{TOPIC}', 'value'),
    Input('select-all', 'n_clicks')
)
def select_all(bar):
    return df.topic.unique() 

@callback(
    Output(f'graph-p-{TOPIC}', 'figure'),
    Input(f'checklist-{TOPIC}', 'value'),
    Input('switch-views', 'value'),
    Input('barmode', 'value')
)
def update_graph(values, views, barmode):
    mask = df.topic.isin(values)
    # dff = df[df.topic==value]
    df["new_p"] = df['shares_avg']
    mode = "group"
    if views == True:
        df["new_p"] = df['shares_avg'] / df['views_avg']
    if barmode == True:
        mode = "stack"
        
    fig = px.bar(df[mask], x='score', y='new_p', color='topic', barmode=mode, color_discrete_sequence=GRAPH_COLORS)
    x_title = "How offensive (0 == not offensive at all, 1 == very offensive)"
    y_title = "P Value"
    style_graph(fig, x_title, y_title)
    return fig


@callback(
    Output(f'graph-count-{TOPIC}', 'figure'),
    Input(f'checklist-{TOPIC}', 'value'),
    Input('barmode', 'value')
)

def update_graph(values, barmode):
    mask = df.topic.isin(values)
    mode = "group"
    if barmode == True:
        mode = "stack"
    fig = px.bar(df[mask], x='score', y='tweet_count', color='topic', barmode=mode, color_discrete_sequence=GRAPH_COLORS)
    x_title = "How offensive (0 == not offensive at all, 1 == very offensive)"
    y_title = "Number of Tweets"
    style_graph(fig, x_title, y_title)
    return fig
