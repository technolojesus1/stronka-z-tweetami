import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from functions.plots_style import style_graph, GRAPH_COLORS, rename_topics, slider, switch, barmode

CSV_PATH = "csv"
TOPIC = "verification"

df = pd.read_csv(f"{CSV_PATH}/{TOPIC}.csv")
rename_topics(df)
# external_stylesheets = [dbc.themes.SANDSTONE]
dash.register_page(__name__, order=5)

layout = dbc.Container([
    dbc.Row([
        html.H2('P Value and Verification Type', className="text-secondary text-center fs-2")
    ]),

    dbc.Button('Select all', id='select-all'),
    dbc.Row([
        dcc.Checklist(
            id=f'checklist-{TOPIC}',
            className="__checklist",
            options=df.topic.unique(),
            value=['Drag Ban'],
            inline=True,
        ),
    ]),

    dbc.Row([
        dcc.Graph(id='graph-verification')
    ]),

    slider("replies"),
    slider("quotes"),
    slider("retweets"),
    switch("views"),
    barmode()

    

], fluid=True)

@callback(
    Output(f'checklist-{TOPIC}', 'value'),
    Input('select-all', 'n_clicks')
)
def select_all(bar):
    return df.topic.unique() 

@callback(
    Output('graph-verification', 'figure'),
    Input(f'checklist-{TOPIC}', 'value'),
    Input('slider-replies', 'value'),
    Input('slider-quotes', 'value'),
    Input('slider-retweets', 'value'),
    Input('switch-views', 'value'),
    Input('barmode', 'value')
)
def update_graph(values, replies, quotes, retweets, views, barmode):
    mask = df.topic.isin(values)
    df["new_p"] = replies * df['reply_average'] + quotes * df['quote_average'] + retweets * df['retweet_average']
    mode = "group"
    if views == False:
        df["new_p"] = df["new_p"] * df["views"]
    if barmode == True:
        mode = "stack"
    # dff = df[df.topic==values]
    fig = px.bar(df[mask], x='type', y='new_p', color='topic', barmode=mode, color_discrete_sequence=GRAPH_COLORS)
    x_title = "Author's verfification type"
    y_title = "P Average"
    style_graph(fig, x_title, y_title)
    return fig
