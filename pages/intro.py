import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from functions.plots_style import style_graph, GRAPH_COLORS, slider, switch, rename_columns, rename_topics

# from app import FONT_FAMILY, FONT_SIZE, GRAPH_COLORS

CSV_PATH = "csv"
TOPIC = "intro"

df = pd.read_csv(f"{CSV_PATH}/{TOPIC}.csv", index_col=0)
rename_columns(df)
rename_topics(df)
df = df.sort_values(by=['Topic'])

table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)

dash.register_page(__name__, order=1)


layout = dbc.Container([
    dbc.Row([
        html.H2('Definitions', className="text-secondary text-center fs-2")
    ]),

    dcc.Markdown('''

    We analyze the parameters derived from tweet content and metadata, obtained through the Twitter API, to identify potential correlations with tweet sharing.
    To measure the likelihood of information being spread further through Twitter's network, we define a value $P_n$ for each tweet $T_n$ in the graph. Specifically, $P_n$ represents the probability of a tweet being retweeted or quoted by another user, and is calculated as 
    $$
    P_n = \\frac{shares(n)}{views(n)}
    $$

    ''', mathjax=True, className="normal-text"),

    

    dbc.Row([
        html.H2('Our Datasets', className="text-secondary text-center fs-2")
    ]),

    dbc.Table(
        table,
        bordered=True,
        dark=True,
        hover=True,
        responsive=True,
        striped=True,
    )
    


], fluid=True)




