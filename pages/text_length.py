import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
from functions.plots_style import style_graph, GRAPH_COLORS, rename_topics, slider, switch

CSV_PATH = "csv"
TOPIC = "text_length"

df = pd.read_csv(f"{CSV_PATH}/{TOPIC}.csv")
rename_topics(df)
# external_stylesheets = [dbc.themes.SANDSTONE]
# app = Dash(__name__, external_stylesheets=external_stylesheets)
dash.register_page(__name__, order=7)

layout = dbc.Container([
    dbc.Row([
        html.H2("P Value and Text Length", className="text-secondary text-center fs-2")
    ]),
    

    dbc.Button('Select all', id='select-all'),
    dbc.Row([
        dcc.Checklist(
            id=f'checklist-{TOPIC}',
            className="__checklist",
            options=df.topic.unique(),
            value=['Drag Ban'],
            inline=True,
        ),
    ]),

    dbc.Row([
        dcc.Graph(id=f'graph-p-{TOPIC}')
    ]),

    dbc.Row([
        dcc.Graph(id=f'graph-count-{TOPIC}')
    ]),


    slider("replies"),
    slider("quotes"),
    slider("retweets"),
    switch("views"),

], fluid=True)

@callback(
    Output(f'checklist-{TOPIC}', 'value'),
    Input('select-all', 'n_clicks')
)
def select_all(bar):
    return df.topic.unique() 

@callback(
    Output(f'graph-p-{TOPIC}', 'figure'),
    Input(f'checklist-{TOPIC}', 'value'),
    Input('slider-replies', 'value'),
    Input('slider-quotes', 'value'),
    Input('slider-retweets', 'value'),
    Input('switch-views', 'value')
)

def update_graph(values, replies, quotes, retweets, views):
    mask = df.topic.isin(values)
    df["new_p"] = replies * df['reply_average'] + quotes * df['quote_average'] + retweets * df['retweet_average']
    if views == False:
        df["new_p"] = df["new_p"] * df["views"]
    # dff = df[df.topic==value]
    fig = px.line(df[mask], x='text_length', y='new_p', color='topic', color_discrete_sequence=GRAPH_COLORS)
    x_title = "Length of Tweet"
    y_title = "P Average"
    style_graph(fig, x_title, y_title)
    return fig


@callback(
    Output(f'graph-count-{TOPIC}', 'figure'),
    Input(f'checklist-{TOPIC}', 'value')
)

def update_graph_count(values):
    mask = df.topic.isin(values)
    # dff = df[df.topic==value]
    fig = px.line(df[mask], x='text_length', y='tweet_count', color='topic', log_y=True, color_discrete_sequence=GRAPH_COLORS)
    x_title = "Length of Tweet"
    y_title = "Number of tweets"
    style_graph(fig, x_title, y_title)
    return fig
