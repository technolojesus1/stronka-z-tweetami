import dash
from dash import Dash, html, dcc, callback, Output, Input
import plotly.express as px
import pandas as pd
import dash_bootstrap_components as dbc
import dash_daq as daq
FONT_FAMILY = 'system-ui, -apple-system, "Segoe UI", Roboto, "Helvetica Neue", "Noto Sans", "Liberation Sans", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"'
FONT_SIZE = 18
# red, green, yellow, blue, magenta, cyan, white, black
NORMAL_COLORS=["#a6cd77","#F08D71","#8589BF","#d35d72","#f0c66f","#354157","#D1CAC7", "#1f1e1c"]
LIGHT_COLORS=["#f86882", "#adda78", "#F3D290", "#81d0c9" , "#9fa0e1", "#F3A690", "#F5F5F4", "#90817b"]
DARK_COLORS=['#a95160','#394634','#E6C075','#404863','#52577A','#F0AA70','#B1A6A2', '#393230']
LARA_COLORS=["#30a6aa", "#fa6b33", "#593196", "#f6c501", "#a2e57b", "#e7337b", "#43794c"]
GRAPH_COLORS=LARA_COLORS

def style_graph(fig, x_title = "", y_title = ""):
    fig.update_layout(
        xaxis_title=x_title,
        yaxis_title=y_title,
        font_size=FONT_SIZE,
        font_family=FONT_FAMILY,
    )

def slider(type):
    slider = dbc.Row([
        dbc.Col([
            dcc.Markdown(f'''change weight of ${type}$:''', mathjax=True, className="normal-text"),
        ], md=3),
        dbc.Col([
            dcc.Slider(0, 1, 0.1,
               value=1,
               id=f'slider-{type}'
            ),
        ], md=7),
    ])

    return slider

def switch(type):
    
    slider = dbc.Row([
        dbc.Col([
            dcc.Markdown(f'''divide by ${type}$?''', mathjax=True, className="normal-text"),
        ], md=3),
        
        dbc.Col([
            daq.ToggleSwitch(
                id=f'switch-{type}',
                value=True,
                className="switch",
                color='#593196'
            ),
        ], md=7),
    ])

    return slider

def barmode():
    
    slider = dbc.Row([
        dbc.Col([
            dcc.Markdown(f'''stack bars?''', mathjax=True, className="normal-text"),
        ], md=3),
        
        dbc.Col([
            daq.ToggleSwitch(
                id=f'barmode',
                value=True,
                className="switch",
                color='#593196'
            ),
        ], md=7),
    ])

    return slider


def rename_columns(df):
    df.rename(columns = {'topic':'Topic', 'tweet_count':'Tweet Count', 'start_date':'Start Date', 'end_date':'End Date'}, inplace = True)

def rename_topics(df):
    df.replace("abortion", "Texas Restricting Abortion", inplace=True)
    df.replace("tiktok", "TikTok Ban", inplace=True)
    df.replace("drag", "Drag Ban", inplace=True)
    df.replace("trump", "Trump's Rape Case", inplace=True)
    df.replace("shooting", "Nashville School Shooting", inplace=True)
    df.replace("writers", "Writers Guild Strike", inplace=True)